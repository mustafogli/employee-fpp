package employeeinfo;

class Account {
    public AccountType accountType;
    private final static double DEFAULT_BALANCE = 0.0;
    private final static double MAX_BALANCE = 1000;
    private double balance;
    private Employee employee;

    Account(Employee emp, AccountType acctType, double balance) {
        employee = emp;
        this.accountType = acctType;
        this.balance = balance;
    }

    Account(Employee emp, AccountType accountType) {
        this(emp, accountType, DEFAULT_BALANCE);
    }

    public String toString() {
        return "Account type: " + accountType + "\nCurrent bal: " + balance;
    }

    public void makeDeposit(double deposit) {
        this.balance += deposit;
    }

    public boolean makeWithdrawal(double amount) {
        if(amount>MAX_BALANCE){
            return false;
        }else{
            this.balance -= amount;
            return true;
        }
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public double getBalance() {
        return balance;
    }
}
