package employeeinfo;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

public class Employee {

	private Account savingsAcct;
	private Account checkingAcct;
	private Account retirementAcct;

	private String name;
	private LocalDate hireDate;
	
	public Employee(String name,int yearOfHire, int monthOfHire, int dayOfHire){
		this.name = name;
		hireDate = LocalDate.of(yearOfHire,monthOfHire,dayOfHire);
	}

	
	public void createNewChecking(double startAmount) {
		checkingAcct = new Account(this,AccountType.CHECKING,startAmount);
	}

	public void createNewSavings(double startAmount) {
		savingsAcct = new Account(this,AccountType.SAVINGS,startAmount);
	}

	public void createNewRetirement(double startAmount) {
		retirementAcct = new Account(this,AccountType.RETIREMENT,startAmount);
	}

	public String getFormattedAcctInfo() {
		StringBuilder strBuilder = new StringBuilder();
		if(checkingAcct!=null)
			strBuilder.append(checkingAcct.toString() + "\n");
		if(savingsAcct!=null)
			strBuilder.append(savingsAcct.toString() + "\n");
		if(retirementAcct!=null)
			strBuilder.append(retirementAcct.toString() + "\n");
		return strBuilder.toString();
	}
	public void deposit(AccountType acctType, double amt){
		switch (acctType){
			case CHECKING:
				checkingAcct.makeDeposit(amt);
				break;
			case SAVINGS:
				savingsAcct.makeDeposit(amt);
				break;
			case RETIREMENT:
				retirementAcct.makeDeposit(amt);
				break;
		}
	}
	public boolean withdraw(AccountType acctType, double amt){
		switch (acctType){
			case CHECKING:
				return checkingAcct.makeWithdrawal(amt);
			case SAVINGS:
				return savingsAcct.makeWithdrawal(amt);
			case RETIREMENT:
				return retirementAcct.makeWithdrawal(amt);
			default:
				return false;
		}
	}

	private String format = "name = $s, salary = %.2f, hireDay = %s";

	public String toString(){
		return String.format(format,name,hireDate.toString());
	}

	public String getName() {
		return name;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}
}
